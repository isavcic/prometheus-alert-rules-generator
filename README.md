# prometheus-alert-rules-generator

Easy alert rules by using simplified YAML files. Sensible defaults. Alerta `correlate` support for different severities.