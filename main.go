package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"

	"github.com/ddspog/colog"
	"github.com/imdario/mergo"
	"github.com/ogier/pflag"
	"gopkg.in/yaml.v2"
)

type PromConfig struct {
	Groups []PromGroup `yaml: groups`
}

type PromGroup struct {
	Name  string     `yaml:"name"`
	Rules []PromRule `yaml:"rules"`
}

type PromRule struct {
	Alert       string            `yaml:"alert"`
	Expr        string            `yaml:"expr"`
	For         string            `yaml:"for"`
	Labels      map[string]string `yaml:"labels"`
	Annotations map[string]string `yaml:"annotations"`
}

type Config struct {
	Name        string            `yaml:"name"`
	Duration    string            `yaml:"duration"`
	Summary     string            `yaml:"summary"`
	Expression  string            `yaml:"expression"`
	Severity    map[string]string `yaml:"severity"`
	Labels      map[string]string `yaml:"labels"`
	Annotations map[string]string `yaml:"annotations"`
}

func init() {
	colog.Register()
}

func main() {

	var src = pflag.StringP("src", "s", "", "Source directory for *.yml config files")
	var dst = pflag.StringP("dst", "d", "", "Destination path where the alert_rules.yml is")
	pflag.Parse()

	if *src == "" || *dst == "" {
		log.Fatalln("error: you have to provide src AND dst parameters!")
	}

	promConfig := PromConfig{}

	files, _ := filepath.Glob(fmt.Sprint(*src, "/*.yml"))

	for _, file := range files {

		log.Println("info: Processing", file)
		configYaml, _ := ioutil.ReadFile(file)
		config := Config{}
		_ = yaml.Unmarshal(configYaml, &config)

		if len(config.Severity) == 0 {
			config.Severity = make(map[string]string)
			config.Severity["critical"] = config.Expression
		}

		var promRules []PromRule

		if config.Name == "" {
			log.Fatalln("error: \"name\" not defined!")
		}

		var correlateSlice []string
		for k := range config.Severity {
			correlateSlice = append(correlateSlice, fmt.Sprintf("%s %s", config.Name, strings.Title(k)))
		}

		correlate := strings.Join(correlateSlice, ",")

		for k, v := range config.Severity {
			promRule := PromRule{}
			promRule.Alert = fmt.Sprintf("%s %s", config.Name, strings.Title(k))
			promRule.Expr = strings.Replace(config.Expression, "$severity", v, -1)
			promRule.For = config.Duration
			promRule.Annotations = make(map[string]string)
			mergo.Merge(&promRule.Annotations, config.Annotations)
			promRule.Annotations["value"] = "{{ $value }}"
			promRule.Annotations["summary"] = strings.Replace(config.Summary, "$value", "{{ $value }}", -1)
			promRule.Labels = make(map[string]string)
			mergo.Merge(&promRule.Labels, config.Labels)
			promRule.Labels["service"] = config.Name
			promRule.Labels["severity"] = k
			if len(config.Severity) > 1 {
				promRule.Labels["correlate"] = correlate
			}
			promRules = append(promRules, promRule)
		}

		promGroup := PromGroup{Name: strings.ToLower(strings.Replace(config.Name, " ", "_", -1)), Rules: promRules}
		promConfig.Groups = append(promConfig.Groups, promGroup)

	}
	// pp.Print(promConfig)
	out, _ := yaml.Marshal(promConfig)
	fmt.Println(string(out))
	ioutil.WriteFile(fmt.Sprint(*dst, "/alert_rules.yml"), out, 0644)
}
